<?php

/**
 * @author Mahmood Babaei
 * @copyright Copyright (c) 2021 Mr.Babaei
 * @license MIT http://opensource.org/licenses/MIT
 */

require_once('DatabaseClass.php');

const JSON_URL = "Events_full.json";

$functionName = filter_input(INPUT_GET, 'action');
switch ($functionName) {
    case 'create-schema':
        createSchema();
        break;
    case 'insert-data':
        insertData();
        break;
    case 'list-data':
        listData();
        break;
    case 'search':
        search();
        break;
    default:
        $data = ['status' => 'Error', 'message' => 'This action not allowed.'];
        $param = urlencode(json_encode($data));
        header("Location: index.php?data={$param}");
}

/**
 * Create database and `event_participants` table.
 */
function createSchema()
{
    $statement = "CREATE DATABASE IF NOT EXISTS dbch CHARACTER SET utf8 COLLATE utf8_general_ci;
     CREATE TABLE IF NOT EXISTS `dbch`.`event_participants` (`participation_id` INT(11) NOT NULL AUTO_INCREMENT , PRIMARY KEY (`participation_id`), `employee_name` VARCHAR(70) NOT NULL , `employee_mail` VARCHAR(255) NOT NULL , `event_id` INT NOT NULL , `event_name` VARCHAR(255) NOT NULL , `participation_fee` DECIMAL(10,2) NOT NULL , `event_date` DATETIME NOT NULL , `version` VARCHAR(50) NOT NULL)";
    $db = new DatabaseClass();
    $resultMessage = $db->Create($statement);
    pageRender($resultMessage);
}

/**
 * read JSON and insert(s) data into event_participants table
 */
function insertData()
{
    $DbClass = new DatabaseClass();
    /** If you want to read JSON from URL change JSON_URL const value  $data */
    $data = file_get_contents(JSON_URL);
    $jsonData = json_decode($data, true);

    $tableName = 'event_participants';
    foreach ((array)$jsonData as $id => $row) {
        $insertPairs = array();
        foreach ((array)$row as $key => $val) {
            if ($key == 'version' && version_compare($val, '1.0.17+60', '<')) {
                $insertPairs['event_date'] = date('Y-m-d H:i:s', strtotime('-60 minutes', strtotime($insertPairs['event_date'])));
            }
            $insertPairs[addslashes($key)] = addslashes($val);
        }
        $insertKeys = '`' . implode('`,`', array_keys($insertPairs)) . '`';
        $insertVals = '"' . implode('","', array_values($insertPairs)) . '"';
        $sql = "INSERT INTO `{$tableName}` ({$insertKeys}) VALUES ({$insertVals});";
        $resultMessage = $DbClass->Insert($sql);
        pageRender($resultMessage);
    }
}

/**
 * Fetch all row from event_participants table
 */
function listData()
{
    $db = new DatabaseClass();
    $statement = 'SELECT * FROM `event_participants`';
    $resultMessage = $db->Select($statement);
    pageRender($resultMessage);
}

/**
 * Search complex value and fetch data
 */
function search()
{
    $db = new DatabaseClass();
    $employeeName = ($_GET['employee_name'] != "") ? $_GET['employee_name'] : null;
    $event_name = ($_GET['event_name'] != "") ? $_GET['event_name'] : null;
    $event_date = ($_GET['event_date'] != "") ? $_GET['event_date'] : null;

    $statement = "Select * from `event_participants` WHERE 1";
    if (!is_null($employeeName))
        $statement .= " and employee_name like '%$employeeName%'";

    if (!is_null($event_name))
        $statement .= " and event_name like '%$event_name%'";

    if (!is_null($event_date))
        $statement .= " and event_date = '$event_date'";

    $resultMessage = $db->Select($statement);
    $out = '';
    $totalFee = 0;
    foreach ($resultMessage['result'] as $item) {

        $out .=   '<tr>
            <th scope="row">' . $item['participation_id'] . '</th>
            <td>' . $item['employee_name'] . '</td>
            <td>' . $item['employee_mail'] . '</td>
            <td>' . $item['event_id'] . '</td>
            <td>' . $item['event_name'] . '</td>
            <td>' . $item['participation_fee'] . '</td>
            <td>' . $item['event_date'] . '</td>
            <td>' . $item['version'] . '</td>
        </tr>';
        $totalFee += $item['participation_fee'];
    }
    $out .= '<tr><th colspan="8" scope="row">Total Fee: ' . $totalFee . '</th></tr>';
    echo  $out;
}

/**
 * By calling this function, can rendering Index page
 * @param $resultMessage
 */
function pageRender($resultMessage)
{
    session_start();
    $_SESSION['data'] = $resultMessage;
    header("Location: /");
}
