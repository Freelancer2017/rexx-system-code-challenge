<!DOCTYPE html>
<html lang="en">

<head>
    <title>Code Challenge </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>
    <style>
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {
            height: 450px
        }

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: #555;
            color: white;
            padding: 15px;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }

            .row.content {
                height: auto;
            }
        }

        .results tr[visible='false'],
        .no-result {
            display: none;
        }

        .results tr[visible='true'] {
            display: table-row;
        }
    </style>
</head>

<body>
<?php
session_start();
$data = isset($_SESSION['data']) ? $_SESSION['data'] : null;
session_destroy();
?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="padding: 10px 15px;" class="navbar-brand" href="#">
                <img style="width: 80px;"
                     class="img-responsive" src="logo.jpg"
                     alt="rexx logo">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li><a href=".">Index</a></li>
                <li><a href="SiteController.php?action=create-schema">Create DB Schema</a></li>
                <li><a href="SiteController.php?action=insert-data">Read Json</a></li>
                <li><a href="SiteController.php?action=list-data">List Data</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-1 sidenav"></div>
        <div class="col-sm-9 text-left">
            <?php if (isset($data['status'])) : ?>
                <div style="margin-top: 10px;" id="alert"
                     class="alert alert-<?= $data['status'] ?>  alert-dismissible show" role="alert">
                    <strong><?= $data['status'] ?>!</strong> <?= $data['message'] ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
            <h1>Szenario:</h1>
            <p>Imagine an external event booking system exports a simple plain json export file (you find the file
                atteched
                to
                this email) of the newest bookings. This data should be presented in our system.</p>
            <hr>
            <?php if (isset($data['list'])) : ?>
                <div class="row">
                    <form class="search">
                        <div class="col-sm-12">
                            <h2>Data filter</h2>
                            <p>You can using this form to multi filter search value.</p>
                        </div>
                        <div class="col-sm-3 form-group">
                            <input type="text" class="form-control" id="employeeName"
                                   value="<?= isset($data['searchParam']['employee_name']) ? $data['searchParam']['employee_name'] : null ?>"
                                   aria-describedby="employeeName" placeholder="Enter Employee Name">
                        </div>
                        <div class="col-sm-3 form-group">
                            <input type="text" class="form-control" id="eventName"
                                   value="<?= isset($data['searchParam']['event_name']) ? $data['searchParam']['event_name'] : null ?>"
                                   placeholder="Event Name">
                        </div>
                        <div class="col-sm-3 form-group">
                            <div class='input-group date' id='dateTimeEvent'>
                                <input value="<?= isset($data['searchParam']['event_date']) ? $data['searchParam']['event_date'] : null ?>"
                                       id="event_date" type='text' class="form-control"/>
                                <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div id="container">
                                <a style="display:inline-block;" id="submit" class="btn btn-primary">Submit</a>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <table class="table table-striped table-responsive results">
                    <thead>
                    <tr>
                        <th data-field="participation_id" scope="col">participation_id</th>
                        <th scope="col">employee_name</th>
                        <th scope="col">employee_mail</th>
                        <th scope="col">event_id</th>
                        <th scope="col">event_name</th>
                        <th scope="col">participation_fee</th>
                        <th scope="col">event_date (UTC)</th>
                        <th scope="col">version</th>
                    </tr>
                    <tr class="warning no-result">
                        <td colspan="8"><i class="fa fa-warning"></i> No result</td>
                    </tr>
                    </thead>
                    <tbody id="body">
                    <?php $totalFee = 0; ?>
                    <?php foreach ($data['result'] as $item) : ?>
                        <tr>
                            <th scope="row"><?= $item['participation_id'] ?></th>
                            <td><?= $item['employee_name'] ?></td>
                            <td><?= $item['employee_mail'] ?></td>
                            <td><?= $item['event_id'] ?></td>
                            <td><?= $item['event_name'] ?></td>
                            <td><?= $item['participation_fee'] ?></td>
                            <td><?= $item['event_date'] ?></td>
                            <td><?= $item['version'] ?></td>
                        </tr>
                        <?php $totalFee += $item['participation_fee']; ?>
                    <?php endforeach; ?>
                    <tr>
                        <th colspan="8" scope="row">Total Fee: <?= $totalFee ?></th>
                    </tr>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
        <div class="col-sm-1 sidenav"></div>
    </div>
</div>

<footer class="container-fluid text-center">
    <p>Mahmood Babaei © <?= date('Y-m-d') ?></p>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#dateTimeEvent').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });
        $('#submit').click(function (e) {
            e.preventDefault();
            $('#body').html('Loading...');
            $.ajax({
                url: 'SiteController.php',
                data: {
                    'action': 'search',
                    'employee_name': $('#employeeName').val(),
                    'event_name': $('#eventName').val(),
                    'event_date': $('#event_date').val(),
                },
                method: 'GET',
                success: function(response) {
                    $('#body').html(response);
                },
            });
        });
    });
</script>
</body>

</html>