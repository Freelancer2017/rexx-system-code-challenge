<?php

/**
 * @author Mahmood Babaei
 * @copyright Copyright (c) 2021 Mr.Babaei
 * @license MIT http://opensource.org/licenses/MIT
 */

class DatabaseClass
{
    const USERNAME = "root";
    const PASSWORD = "123";

    private $connection = null;

    // this function is called everyTime this class is instantiated
    public function __construct($dbHost = "localhost", $username = self::USERNAME, $password = self::PASSWORD)
    {
        $dbName = $_GET['action'] == "create-schema" ? "" : "rexx";
        try {
            $this->connection = new PDO("mysql:host={$dbHost};dbname={$dbName};", $username, $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        } catch (Exception $e) {
            return ["status" => 'danger', "message" => $e->getMessage()];
        }

    }

    // Insert a row/s in a Database Table
    public function Insert($statement = "")
    {
        try {
            $this->executeStatement($statement);
            return ["status" => 'success', "message" => "JSON Data imported successfully."];

        } catch (Exception $e) {
            return ["status" => 'danger', "message" => $e->getMessage()];
        }
    }

    // Select a row/s in a Database Table
    public function Select($statement = "")
    {
        try {
            $stmt = $this->executeStatement($statement);
            return ["list" => true, "result" => $stmt->fetchAll()];
        } catch (Exception $e) {
            return ["status" => 'danger', "message" => $e->getMessage()];
        }
    }

    //  Create Database and Table
    public function Create($statement = "")
    {
        try {
            $this->executeStatement($statement);
            return ["status" => 'success', "message" => "Database created successfully"];
        } catch (Exception $e) {
            return ["status" => 'danger', "message" => $e->getMessage()];
        }
    }

    // execute statement
    private function executeStatement($statement = "")
    {
        try {
            $stmt = $this->connection->prepare($statement);
            $stmt->execute();
            return $stmt;

        } catch (Exception $e) {
            return ["status" => 'danger', "message" => $e->getMessage()];
        }
    }

}
